#pragma once

#include <KQuickManagedConfigModule>

class StateBinding : public KQuickManagedConfigModule
{
    Q_OBJECT

public:
    StateBinding(QObject *parent, const KPluginMetaData &);
    ~StateBinding() override;
};
