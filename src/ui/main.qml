import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2

import org.kde.kirigami as Kirigami
import org.kde.kcmutils as KCM

KCM.SimpleKCM {
    id: root

    implicitWidth: Kirigami.Units.gridUnit * 44
    implicitHeight: Kirigami.Units.gridUnit * 25

    ColumnLayout {
        anchors.fill: parent

        Loader {
            id: contentLoader
            Layout.fillWidth: true
            sourceComponent: _cTest
        }

        QQC2.Button {
            text: "Reload Loader"
            onClicked: {
                contentLoader.sourceComponent = null;
                contentLoader.sourceComponent = _cTest;
            }
        }
    }

    Component {
        id: _cTest

        Test {}
    }
}
