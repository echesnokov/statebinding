import QtQuick
import QtQuick.Layouts
import QtQuick.Controls as QQC2

import org.kde.kquickcontrols as KQuickControls
import org.kde.kirigami as Kirigami
import org.kde.kcmutils as KCM

import org.kde.kcmutils.private as P

Kirigami.FormLayout {
    Item {
        Kirigami.FormData.isSection: true
        Kirigami.FormData.label: "highlight: spinBox.value === 50\nAll this components inside Loader"
    }

    QQC2.Button {
        id: button
        text: "Nothing"
        implicitWidth: 200

        P.SettingHighlighterPrivate {
            id: settingHighlighterPrivate
            highlight: spinBox.value === 50
            target: button
            defaultIndicatorVisible: kcm.defaultsIndicatorsVisible
        }
    }

    QQC2.SpinBox {
        id: spinBox
        implicitWidth: 200
        value: 50

        P.SettingHighlighterPrivate {
            highlight: spinBox.value === 50
            target: spinBox
            defaultIndicatorVisible: kcm.defaultsIndicatorsVisible
        }
    }

    QQC2.Slider {
        id: slider
        from: spinBox.from
        to: spinBox.to
        value: spinBox.value
        implicitWidth: 200

        KCM.SettingHighlighter {
            highlight: spinBox.value === 50
        }
    }

    QQC2.TextArea {
        id: textArea
        implicitWidth: 200
        implicitHeight: 50

        P.SettingHighlighterPrivate {
            highlight: spinBox.value === 50
            target: textArea
            defaultIndicatorVisible: kcm.defaultsIndicatorsVisible
        }
    }

    QQC2.TextField {
        id: textField
        implicitWidth: 200

        P.SettingHighlighterPrivate {
            highlight: spinBox.value === 50
            target: textField
            defaultIndicatorVisible: kcm.defaultsIndicatorsVisible
        }
    }

    // KQuickControls.KeySequenceItem {
    //     id: alternativeShortcut
    //     modifierlessAllowed: false


    //     KCM.SettingHighlighter {
    //         target: alternativeShortcut
    //         highlight: true
    //     }
    // }
}
