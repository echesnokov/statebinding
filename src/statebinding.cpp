#include "statebinding.h"

K_PLUGIN_FACTORY_WITH_JSON(StateBindingFactory, "statebinding.json", registerPlugin<StateBinding>();)

StateBinding::StateBinding(QObject *parent, const KPluginMetaData &metaData)
    : KQuickManagedConfigModule(parent, metaData)
{
}

StateBinding::~StateBinding()
{
}

#include "moc_statebinding.cpp"
#include "statebinding.moc"
